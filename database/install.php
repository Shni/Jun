<?php
include '../libs/Config.php';
include '../libs/DB.php';

$config = new Config('../config.ini');
$connection = new DB($config);
$connection->exec('CREATE TABLE  countries (
                                            id INT PRIMARY KEY AUTO_INCREMENT,
                                            country_name VARCHAR(255)
                                             )');

$connection->exec('CREATE TABLE users (
                                             id INT PRIMARY KEY AUTO_INCREMENT,
                                             user_name VARCHAR(255) NOT NULL,
                                             user_email VARCHAR(255) NOT NULL,
                                             user_country_id INT,
                                             FOREIGN KEY (user_country_id) REFERENCES countries(id)
                                             )');