<?php
include '../libs/User.php';
include '../libs/DB.php';
include '../libs/Config.php';



    if (empty($_POST['user_name']) || empty($_POST['user_email'])) {
        echo "Ці поля обов'язкові до заповнення!";
    } else {
        $config = new Config('../config.ini');
        $connection = new DB($config);
        $user = new User($connection);

        $id = $_POST['id'];
        $name = $connection->quote($_POST['user_name']);
        $email = $connection->quote($_POST['user_email']);
        $country = $connection->quote($_POST['country_id']);

        if ($_POST['hidden'] == 'add') {
            echo $user->addUsers($name, $email, $country);
        }elseif ($_POST['hidden'] == 'update'){
            echo $user->updateUsers($name, $email, $country, $id);
        }
}