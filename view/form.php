<!DOCTYPE html>
<html>
<head>
    <meta charset="utf8">
    <title>Junior</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.4.1/css/bulma.css">
    <style type="text/css">
        body {
            padding-top: 50px;
        }
    </style>
</head>
<body class="container">

<?php
include '../libs/DB.php';
include '../libs/User.php';
include '../libs/Config.php';
include '../libs/Countrie.php';

$config = new Config('../config.ini');
$connection = new DB($config);
$user = new User($connection);
$countries = new Countrie($connection);
$countries = $countries->getAll();

if ($_GET['page'] == 'add') {
    $title = "Додати Користувача";
    $user_name = '';
    $user_email = '';
    $user_country_id = '';
    $button = 'Додати';
    $hidden = 'add';
} else if ($_GET['page'] == 'update') {
    $title = "Редагувати Користувача";
    $button = 'Редагувати';
    $hidden = 'update';
    $pd = $user->preUpdateUsers($_GET['id']);

    foreach ($pd as $personalDetails) {
        $id = $personalDetails['id'];
        $user_name = $personalDetails['user_name'];
        $user_email = $personalDetails['user_email'];
    }
}
?>
<h1 class="title"><?= $title ?></h1>
<hr>
<form method="POST" action="../process/processForm.php">
    <div class="field">
        <label class="label">Name</label>
        <p class="control">
            <input class="input" name="user_name" type="text" value="<?= $user_name ?>">
        </p>
    </div>
    <div class="field">
        <label class="label">Email</label>
        <p class="control">
            <input class="input" name="user_email" type="text" value="<?= $user_email ?> ">
        </p>
    </div>
    <div class="field">
        <label class="label">Country</label>
        <p class="control">
            <span class="select">
               <select name="country_id">
                  <?php foreach ($countries as $country) : ?>
                      <option value="<?= $country['id'] ?>"><?= $country['country_name'] ?></option>
                  <?php endforeach; ?>
               </select>
            </span>
        </p>
    </div>
    <input type="hidden" name="hidden" value="<?= $hidden ?>">
    <input type="hidden" name="id" value="<?= $id ?>">
    <button class="button is-success" type="submit"><?= $button ?> </button>
</form>
</body>

</html>