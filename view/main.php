
<?php
include 'libs/DB.php';
include 'libs/User.php';
include 'libs/Config.php';

$config = new Config('config.ini');
$connection = new DB($config);
$user =  new User($connection);
$users = $user->showUsers();


?>

<div  class="block">
    <a href="view/form.php?page=add" class="button is-info">Додати Користувача</a>
</div>
<div>
    <?php foreach ($users as $user) : ?>
        <?php if($user['user_name'] !== NULL) :?>
        <div class="card">
            <header class="card-header">
                <p class="card-header-title">
                    Картка користувача
                </p>
                <a class="card-header-icon">
      <span class="icon">
        <i class="fa fa-angle-down"></i>
      </span>
                </a>
            </header>
            <div class="card-content">
                <div class="content">
                    <ul>
                        <li> Ім'я: <?=  $user['user_name']; ?></li>
                        <li> Електронна почта: <?=  $user['user_email']; ?></li>
                        <li> Країна: <?=  $user['country_name']; ?></li>
                    </ul>
                </div>
            </div>
            <footer class="card-footer">
                <a href="process/deleteUser.php?id=<?= $user['id'] ?>" class="card-footer-item">Видалити</a>
                <a href="view/form.php?id=<?= $user['id'] ?>&page=update" class="card-footer-item">Редагувати</a>
            </footer>
        </div>
            <?endif; ?>
    <?php endforeach; ?>



</div>

