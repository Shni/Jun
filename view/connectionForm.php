<!DOCTYPE html>
<html>
<head>
    <meta charset="utf8">
    <title>Junior</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.4.1/css/bulma.css">
    <style type="text/css">
        body {
            padding-top: 50px;
        }
    </style>
</head>
<body class="container">
<h1 class="title"> Встновіть з'єднання з базою даних </h1>
<hr>
<form method="POST" action="../process/insertInConfig.php">
    <div class="field">
        <label class="label">DB_Name</label>
        <p class="control">
            <input class="input" name="DB_Name" type="text" placeholder='1' ">
        </p>
    </div>
    <div class="field">
        <label class="label">Host</label>
        <p class="control">
            <input class="input" name="Host" type="text" placeholder="2" ">
        </p>
    </div>
    <div class="field">
        <label class="label">DB_User</label>
        <p class="control">
            <input class="input" name="DB_User" type="text" placeholder="3" ">
        </p>
    </div>
    <div class="field">
        <label class="label">DB_Password</label>
        <p class="control">
            <input class="input" name="DB_Password" type="text" placeholder="2" ">
        </p>
    </div>
    <button class="button is-success" type="submit"> Встановити зєднання </button>
</form>
</body>

</html>