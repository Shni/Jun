
<?php

class Config
{
    function __construct($filename)
    {
        $this->filename = $filename;
    }

    function get($str)
    {
        $parse = parse_ini_file($this->filename);
        return $parse[$str];
    }

    function insert($host, $db_name, $db_user, $db_password)
    {
        return file_put_contents($this->filename, [$host, $db_name, $db_user, $db_password]);
    }
}
