<?php

class DB
{

    public function __construct(Config $config)
    {
        return $this->connection = new PDO("mysql:host={$config->get('Host')};dbname={$config->get('DB_Name')}", $config->get('DB_User'), $config->get('DB_Password'));
    }

    public function exec($param)
    {
        return $this->connection->exec($param);
    }

    public function fetch($param)
    {
        $fetch = $this->connection->prepare($param);
        $fetch->execute();
        return $fetch->fetchAll(PDO::FETCH_ASSOC);
    }

    public function quote($param)
    {
        return $this->connection->quote($param);
    }
}



