<?php


class User
{
    function __construct(DB $connection)
    {
        $this->connection = $connection;
    }

    function showUsers()
    {
        return $this->connection->fetch("SELECT * FROM countries
                                                    LEFT JOIN users ON countries.id = users.user_country_id");

    }

    function addUsers($name, $email, $country_id)
    {
        $this->connection->exec("INSERT INTO users (user_name, user_email, user_country_id)  
                                                           VALUES ($name, $email, $country_id)");
        return 'Користувача додано';
    }

    function preUpdateUsers($id)
    {
        return $this->connection->fetch("SELECT * FROM users WHERE id = $id");
    }

    function updateUsers($name, $email, $country_id, $id)
    {
       $this->connection->exec("UPDATE users 
                                        SET user_name = $name, user_email = $email, user_country_id = $country_id
                                        WHERE id = $id ");
       return 'Всьо ок';
    }

    function deleteUsers($id)
    {
         $this->connection->exec("DELETE FROM users WHERE id = $id");
         return 'Користувача видалено';
    }


}
