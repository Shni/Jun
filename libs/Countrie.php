<?php

class Countrie
{
    function __construct(DB $connection)
    {
        $this->connection = $connection;
    }

    function getAll() {
        return $this->connection->fetch("SELECT * FROM test.countries");
    }
}